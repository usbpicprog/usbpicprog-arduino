#include <xc.h>
#include <stdint.h>
#include <libpic30.h>

/* oscillator settings */
#pragma config FCKSM = CSW_FSCM_OFF 	// Clock Switching and Monitor disabled
#pragma config FNOSC = FRC    		// Internal oscillator
#pragma config POSCMD = PRIOSC_OFF
#pragma config OSCIOFNC = OSC2_IO
#pragma config FRANGE = FRC_HI_RANGE
#pragma config FPWRT = PWRT_128

/* ICS selection */
#pragma config ICS = ICS_PGD

/* code and boot section protection - off */
#pragma config BWRP = BWRP_OFF
#pragma config GWRP = GWRP_OFF		// Writes to program memory allowed
#pragma config GSS = GSS_OFF		// Code protection is disabled

/* WDT disabled */
#pragma config FWDTEN = FWDTEN_OFF   	// Watchdog Timer disabled

int main()
{
	/* configure PORTA as output, pin 16 of dsPIC30F 28pin SDIP*/
   	ADPCFG = 0xFFFF; // set to digital I/O (not analog)
	_TRISA9 = 0;
	while(1) {
		_LATA9 = 1;
		__delay32((uint32_t)1000*1000*4);
		_LATA9 = 0;
		__delay32((uint32_t)1000*1000*4);
	}
	return 0;
}
