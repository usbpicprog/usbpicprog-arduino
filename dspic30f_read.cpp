#include "Arduino.h"
#include "setup.h"
#include "helpers.h"

#include "dspic30f_io.h"
#include "dspic30f_asm.h"

/*
 * The serial instruction execution code is taken from the
 * Microchip Flash Programming specification document, DS70284C
 */

/**
 * Reads a 16-bit word from program space with the given src offset. The offset
 * is a 32-bit value from which only the least significant 24-bits are used.
 * The offset is specified in byte offset aligned by 2. This routine can be
 * used to read values from program space 0x00_0000 to 0xFF_FFFE. The
 * configuration words of the dsPIC30F family typically ranges from
 * 0xF8_0000 to 0xF8_000E.
 * The output value is returned as register out when the code is executed.
 *
 */
#define READ_CONFIG_WORD(_src) \
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(NOP),	\
	\
	SIX(MVI16((_src) >> 16, W0)), /* MOV #0xFF, W0 */ \
	SIX(0x880190), /* MOV W0, TBLPAG */ \
	SIX(MVI16((_src), W0)), /* MOV #0x0, W0 */ \
	SIX(0x207841), /* MOV VISI, W1 */ \
	SIX(0xBA0890), /* TBLRDL [W0], [W1] */ \
	SIX(NOP), \
	SIX(NOP), \
	\
	REG_OUT, /* VISI out */\
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(NOP) /* NOP */




/**
 * Read 4 program instructions from user memory space.
 * _src: The source points to the byte offset of the program word to be read
 * 	 and is aligned by 2. In the dsPIC family the program words are
 * 	 24-bit long but the offsets are provided in multiples of 2.
 *
 * Notes: The first instruction is at offset 0 and occupies 3 bytes and the
 * second instruction is at offset 2, third one at offset 4 and so on.
 * Conceptually this can be thought as 2 banks of memory at the same higher
 * offset.
 *
 * +------------+ 00_0000           +------------+ 00_0000
 * |  I0[15:0]  |                   |  I0[23:16] |
 * +------------+ 00_0002           +------------+ 00_0002
 * |  I1[15:0]  |                   |  I1[23:16] |
 * +------------+ 00_0006           +------------+ 00_0006
 * |  I2[15:0]  |                   |  I2[23:16  |
 * +------------+ 00_0008           +------------+ 00_0008
 * |            |                   |            |
 * ..           ..                  ..           ..
 *
 * At each offset, the lower
 * and higher program words are accessed with different instructions allowing
 * to read the extra bits [23:16]. With a given byte offset, the higher
 * program word (23:16) is accessed through a TBLRDH and the lower
 * program word (15:0) is accessed through TBLRL instruction.
 */
#define READ_4_PROGRAM_WORDS(_src) \
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(NOP), \
	\
	SIX(MVI16((_src) >> 16, W0)), /* MOV #0xFF, W0 */ \
	SIX(0x880190), /* MOV W0, TBLPAG */ \
	SIX(MVI16((_src), W6)), /* MOV #0x0, W6 */ \
	SIX(0xEB0380),	/* CLR W7 */ \
	\
	SIX(0xBA1B96),	/* TBLRDL [W6], [W7++] */ \
	SIX(NOP), \
	SIX(NOP), \
	\
	SIX(0xBADBB6), /* TBLRDH.B [W6++], [W7++] */ \
	SIX(NOP), \
	SIX(NOP), \
	\
	SIX(0xBADBD6), /* TBLRDH.B [++W6], [W7++] */ \
	SIX(NOP), \
	SIX(NOP), \
	\
	SIX(0xBA1BB6), /* TBLRDL [W6++], [W7++] */ \
	SIX(NOP), \
	SIX(NOP), \
	\
	SIX(0xBA1B96),	/* TBLRDL [W6], [W7++] */ \
	SIX(NOP), \
	SIX(NOP), \
	\
	SIX(0xBADBB6), /* TBLRDH.B [W6++], [W7++] */\
	SIX(NOP), \
	SIX(NOP), \
	\
	SIX(0xBADBD6), /* TBLRDH.B [++W6], [W7++] */\
	SIX(NOP), \
	SIX(NOP), \
	\
	SIX(0xBA0BB6),/* TBLRDL [W6++], [W7] */ \
	SIX(NOP), \
	SIX(NOP), \
	\
	CLK_OUT_REG(W0), \
	CLK_OUT_REG(W1), \
	CLK_OUT_REG(W2), \
	CLK_OUT_REG(W3), \
	CLK_OUT_REG(W4), \
	CLK_OUT_REG(W5), \
	SIX(NOP), \
	SIX(0x40100), \
	SIX(NOP)



/*
 * Execute instructions on target for reading a 16-bit configuration word
 * offb: Byte offset aligned to 2 bytesa, 0x00_0000 to 0xFF_FFFE
 * Return: 16-bit value is returned
 */
uint16_t read_config_word(unsigned long offb)
{
	unsigned long instrs[] = {
		READ_CONFIG_WORD(offb),
	};
	return execute_code(instrs, ARRAY_SIZE(instrs));
}


/*
 * Execute instructions on target for reading 4-program words.
 * offw: The program word offset in bytes aligned by 2.
 * data: data buffer where the output will be written, the buffer needs to
 *  	 be atleast 12 bytes long. Each program word occupies 3 bytes even
 *  	 though the offset is incremented by 2. See notes on the program
 *  	 instructions. The data output will be arranged sequentially where
 *  	 the first 3 bytes is program word 1 and the next 3 bytes, program
 *  	 word 2 and so on.
 */
void read_4_program_words(unsigned long offw, uint8_t *data)
{
	unsigned long instrs[] = {
		READ_4_PROGRAM_WORDS(offw)
	};

	/*
	 * The data is returned in the packed format as follows:
	 * W0L W0M W0H W1H W1L W1M, ...
	 * The unpack routine changes to the  required
	 * W0L W0M W0H W1L W1M W1H, ...
	 */
	execute_code_outbuff(instrs, ARRAY_SIZE(instrs), (uint16_t *)data, 6);
	/*
	 * Host programmer expects the data in packed format, so
	 * return the data as is.
	 * UNPACK_INSTR_WORDS(data);
	 */
}

/*
 * Used to read user program memory.
 * offb: Offset in bytes aligned by 2.
 * data: Data buffer to store the output.
 * nbytes: Size of the output buffer in bytes.
 * Notes:
 *  The buffer size must be a multiple of 12 and will be rounded off
 * to the nearest multiple.
 * Return: The function returns the number of bytes actually read.
 *
 */
unsigned int read_user_memory(unsigned long offb, uint8_t *data,
							unsigned int nbytes)
{
	unsigned int i;
	unsigned long offw;

	/* offb + nbytes can never be < DSPIC_CONFIG_MEM_START (0) */
	/* check the maximum extents */
	if(offb + nbytes > DSPIC_USER_MEM_END) {
		prln("User memory region is out of bounds");
		return 0;
	}
	/* convert the byte offset to program word offset */
	offw = offb*2/3;
	/* the size will be floored to multiples of 12 bytes */
	for(i=0; i < nbytes/12; i++) {
		read_4_program_words(offw + i*8, data + i*12);
	}

	return i*12;
}

/*
 * Used to read configuration memory.
 * offb: Offset in bytes aligned by 2.
 * data: Data buffer to store the output.
 * nbytes: Size of the output buffer in bytes.
 * Notes:
 *  The buffer size must be a multiple of 2 and will be rounded off
 * to the nearest multiple.
 * Return: The function returns the number of bytes actually read.
 *
 */
unsigned int read_config_memory(unsigned long offb, uint8_t *data,
							unsigned int nbytes)
{
	unsigned int i;
	/*
	 * The dsPIC config words are 16bit values
	 */

	if(offb + nbytes < DSPIC_CONFIG_MEM_START ||
		offb + nbytes > DSPIC_CONFIG_MEM_END) {
		prln("Config memory region is out of bounds");
		return 0;
	}

	/* the size will be floored to multiples of 2 bytes */
	for(i=0; i < nbytes/2; i++) {
		*((uint16_t *)data + i) = read_config_word(offb + i*2);
	}

	return i*2;
}

/*
 * Used to read program memory which could be either
 * configuration or user memory but not both.
 * offb: Offset in bytes aligned by 2.
 * data: Data buffer to store the output.
 * nbytes: Size of the output buffer in bytes.
 * Notes:
 *  The buffer size must be multiple of 2 and min of 2 for configuration
 *  space read. For user program memory the size must be a multiple of 2
 *  and min of 12. In either cases the size will be rounded to
 *  the nearest multiple.
 * Return: The function returns the number of bytes actually read.
 *
 */

unsigned int read_program_memory(unsigned long offb, uint8_t *data,
							unsigned int nbytes)
{
	unsigned int ret = 0;
	/*
	 * check if the requested area exclusively belongs to a particular
	 * region.
	 */
	if(offb + nbytes < DSPIC_USER_MEM_END) {
		/* the entire requested area resides in user memory */
		ret = read_user_memory(offb, data, nbytes);
	/* offb + nbytes >= DSPIC_USER_MEM_END */
	} else if (offb >= DSPIC_CONFIG_MEM_START) {
		/* area is in config space */
		ret = read_config_memory(offb, data, nbytes);
	}
	return ret;
}
/*
 * Target program to check if the target is functional.
 * Currently this routine is used to arithmetic right the input by 1 position.
 * The input and output are 16-bit values.
 */
#define TARGET_ASR(_val) \
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(NOP), \
	\
	SIX(MVI16((_val), W0)),   \
	SIX(0xD18000), /* asr.w     w0, w0 */ \
	\
	CLK_OUT_REG(W0), \
	SIX(NOP), \
	SIX(NOP)

unsigned int target_asr(unsigned int val)
{
	unsigned long instrs[] = {
		TARGET_ASR(val)
	};

	return execute_code(instrs, ARRAY_SIZE(instrs));
}

