#ifndef __LOG_H__
#define __LOG_H__
/*
 * Include this file before any other includes to get the defines effective
 * for that source file.
 */


/*
 * If none of the below are defined, the pr statements will not be included
 * PRINT_DEBUG		-  used to always print the debug message
 * PRINT_DYNAMIC	-  print dynamically if set using the CMD_DEBUG
 *
 * Note that either of the above options will increase the binary size
 */

/* global dynamic print */
extern int dynamic_print;
void set_dynamic_print(int en);

#ifdef PRINT_DEBUG
#define _COND		1 	/* expecting compiler to optimize out */
#elif defined(PRINT_DYNAMIC)
#define _COND		dynamic_print
#endif

#if  defined(PRINT_DEBUG) || defined (PRINT_DYNAMIC)
#define pr(...)		if(_COND) { Serial.print(__VA_ARGS__); }
#define prln(...) 	if(_COND) { Serial.println(__VA_ARGS__); }

#define prhex(_s, _v) 	if(_COND) { \
				Serial.print(_s); \
				Serial.print("0x"); \
				Serial.println(_v, HEX); \
			}

#define prinfo(_s, _v) 	if(_COND) { \
				Serial.print(_s); \
				Serial.println(_v); \
			}
#else // !PRINT_SERIAL && !PRINT_DYNAMIC
#define pr(...)
#define prln(...)
#define prhex(_s, _v)
#define prinfo(_s, _v)

#endif	// PRINT_SERIAL || PRINT_DYNAMIC

void dump_data(const char *msg, const uint8_t *data, uint32_t off, int size);
#endif // __LOG_H__
