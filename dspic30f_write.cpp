#include "Arduino.h"
#include "setup.h"
#include "helpers.h"

#include "dspic30f_asm.h"
#include "dspic30f_io.h"

/*
 * The serial instruction execution code is taken from the
 * Microchip Flash Programming specification document, DS70284C
 */

/* erase/write helpers */
/* unlocks NMCON operation */
#define UNLOCK_NVMCON \
		SIX(MVI8(0x55, W8)),	/* MOV #0x55, W8 */	\
		SIX(0x883B38 ),		/* MOV W8, NVMKEY */	\
		SIX(MVI8(0xAA,W9) ), 	/* MOV #0xAA, W9 */ 	\
		SIX(0x883B39)		/* MOV W9, NVMKEY */

/* initiates and completes NVMCOM operation */
#define DO_NVMCON_OP \
		SIX(0xA8E761),	/* BSET NVMCON, #WR */ \
		SIX(NOP),  \
		SIX(NOP),  \
		SIX(NOP),  \
		SIX(NOP),  \
		MDELAY(4),	/* External delay P19a/P18a, 4ms */ \
		SIX(0xA9E761), 	/* BCLR NVMCON, #WR */ \
		SIX(NOP),  \
		SIX(NOP),  \
		SIX(NOP),  \
		SIX(NOP)

/* ends a full program by resetting PC to user start */
#define END_CODE	\
	SIX(0x040100), /* GOTO 0x100 */ \
	SIX(NOP) /* NOP */



/* Erase the entire device */
 /* An erase operation on user memory space will set the bytes to FF
  * whereas an erase on the configuration space will set the values
  * to the default values.
  */
#define ERASE_MEM() \
	SIX(0x040100), /* GOTO 0x100 */ \
	SIX(0x040100), /* GOTO 0x100 */ \
	SIX(NOP), /* NOP */ \
	SIX(MVI16(NVMCON_ERASE_ALL, W10)), /* MOV #_op, W10 */ \
	SIX(0x883B0A), /* W10, NVMCON */ \
	UNLOCK_NVMCON, \
	DO_NVMCON_OP


void erase_all()
{
	unsigned long instrs[] = {
		ERASE_MEM(),
	};
	execute_code(instrs, ARRAY_SIZE(instrs));

}


/* Write helpers */

/* Write words in Data memory to the program latch
 * W0-W5 	- program bytes (4 instrs) to be written
 * TBLPAG 	- high address[23:16] of destination
 * W7 		- low[15:0] address of destination
 * clobbers: W6, W7
 */
#define MOV_4IWORDS_LATCH \
	SIX(0xEB0300), /* CLR W6 */ \
	SIX(NOP), \
	SIX(0xBB0BB6), /* TBLWTL [W6++], [W7] */ \
	SIX(NOP), \
	SIX(NOP), \
	SIX(0xBBDBB6), /* TBLWTH.B [W6++], [W7++] */ \
	SIX(NOP), \
	SIX(NOP), \
	SIX(0xBBEBB6), /* TBLWTH.B [W6++], [++W7] */ \
	SIX(NOP), \
	SIX(NOP), \
	SIX(0xBB1BB6), /* TBLWTL [W6++], [W7++] */ \
	SIX(NOP), \
	SIX(NOP), \
	SIX(0xBB0BB6), /* TBLWTL [W6++], [W7] */ \
	SIX(NOP), \
	SIX(NOP), \
	SIX(0xBBDBB6), /* TBLWTH.B [W6++], [W7++] */ \
	SIX(NOP), \
	SIX(NOP), \
	SIX(0xBBEBB6), /* TBLWTH.B [W6++], [++W7] */ \
	SIX(NOP), \
	SIX(NOP), \
	SIX(0xBB1BB6), /* TBLWTL [W6++], [W7++] */ \
	SIX(NOP), \
	SIX(NOP)


/*
 * Program to write 32 instruction words to a memory row.
 * The write operations are always 32 instruction or 96 bytes aligned.
 */
#define WRITE_SETUP(_dest) \
	SIX(0x040100), /* GOTO 0x100 */ \
	SIX(0x040100), /* GOTO 0x100 */ \
	SIX(NOP), /* NOP */ \
	\
	/* set NVMCON operation */ \
	SIX(MVI16(NVMCON_WRITE_32IWORDS, W10)), \
	SIX(0x883B0A), /* MOV W10, NVMCON */ \
	/* setup destination address */ \
	SIX(MVI8(_dest >> 16, W0)), \
	SIX(0x880190),	/* MOV W0, TBLPAG */ \
	SIX(MVI16(_dest, W7))

/* load 12 bytes of data in packed format to the write latch*/
/*
 * Fill in the code bytes into instruction immediate value.
 * This routine will be used to load the immediate values
 * into registers and from registers into program latch.
 * Uses registers W0 - W5.
 */
#define WRITE_4INSTR_WORDS(_code) \
	SIX(MVI(_code[W0L], _code[W0M], W0)), \
	SIX(MVI(_code[W0H], _code[W1H], W1)), \
	SIX(MVI(_code[W1L], _code[W1M], W2)), \
	SIX(MVI(_code[W2L], _code[W2M], W3)), \
	SIX(MVI(_code[W2H], _code[W3H], W4)), \
	SIX(MVI(_code[W3L], _code[W3M], W5)), \
	MOV_4IWORDS_LATCH

/* The data is already packed, just use the buffer as is */
#define WRITE_PACKED_4INSTR_WORDS(_code) \
	SIX(MVI(_code[W0L], _code[W0M], W0)), \
	SIX(MVI(_code[W0H], _code[W1L], W1)), \
	SIX(MVI(_code[W1M], _code[W1H], W2)), \
	SIX(MVI(_code[W2L], _code[W2M], W3)), \
	SIX(MVI(_code[W2H], _code[W3L], W4)), \
	SIX(MVI(_code[W3M], _code[W3H], W5)), \
	MOV_4IWORDS_LATCH

#define WRITE_FINISH() \
	/* unlock and program */ \
	UNLOCK_NVMCON, \
	DO_NVMCON_OP,\
	END_CODE

static void write_setup(unsigned long offb)
{
	unsigned long instrs[] = {
		WRITE_SETUP(offb),
	};
	execute_code(instrs, ARRAY_SIZE(instrs));
}

static void write_4instr_words(uint8_t *code)
{
	unsigned long instrs[] = {
		WRITE_PACKED_4INSTR_WORDS(code),
	};
	execute_code(instrs, ARRAY_SIZE(instrs));
}

static void write_finish()
{
	unsigned long instrs[] = {
		WRITE_FINISH(),
	};
	execute_code(instrs, ARRAY_SIZE(instrs));
}


void write_32instr_words(unsigned long offb, uint8_t *data)
{
	int i;
	write_setup(offb);
	/* repeat for 32 instructions */
	for(i=0; i<8; i++)
		write_4instr_words(data + i*12);

	write_finish();
}

/*
 * write a 16bit config value to the destination. The
 * configuration value can be written without erasing and
 * the reserved or fixed values cannot be written/erased.
 */
#define WRITE_CONFIG_WORD(_dest, _val) \
	SIX(0x040100), /* GOTO 0x100 */ \
	SIX(0x040100), \
	SIX(NOP), \
	/* setup nvmcon operation */\
	SIX(MVI16(NVMCON_WRITE_CONFIG, W10)), \
	SIX(0x883B0A), /* MOV W10, NVMCON */ \
	/* set destination location */ \
	SIX(MVI16(_dest, W7)), \
	SIX(MVI8((_dest)>>16, W0)), \
	SIX(0x880190), /* MOV W0, TBLPAG */ \
	/* set data word and write latch */\
	SIX(MVI16((_val), W6)), \
	SIX(0xBB1B86), /* TBLWTL W6, [W7++] */ \
	\
	SIX(NOP), \
	SIX(NOP), \
	/* unlock and perform the write operation */\
	UNLOCK_NVMCON, \
	DO_NVMCON_OP, \
	END_CODE

void write_config_word(unsigned long dest, uint16_t val)
{
	unsigned long instrs[] = {
		WRITE_CONFIG_WORD(dest, val),
	};
	execute_code(instrs, ARRAY_SIZE(instrs));
}


unsigned int write_user_memory(unsigned long offb, uint8_t *data,
							unsigned int nbytes)
{
	unsigned int i;
	unsigned long offw;

	/* offb + nbytes can never be < DSPIC_CONFIG_MEM_START (0) */
	/* check the maximum extents */
	if((offb + nbytes) > DSPIC_USER_MEM_END) {
		prln("User memory region is out of bounds");
		return 0;
	}

	/* Each program word is at a 2 byte offset but occupies
	 * 3 bytes of memory. See the read function for more details
	 * on this.
	 */
	/* convert the byte offset to program word offset */
	offw = offb*2/3;

	/* the offsets must be 64 byte aligned */
	if(offw % 64 != 0) {
		prln("Invalid offset for memory programming");
		return 0;
	}
	/* the size will be floored to multiples of 96 bytes */
	for(i=0; i < nbytes/96; i++) {
		write_32instr_words(offw + i*64, data + i*96);
	}

	return i*96;

}
