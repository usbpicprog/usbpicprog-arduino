#ifndef _UPP_H_
#define _UPP_H_
#define MAKE_UL(_a,_b,_c,_d)	((uint32_t)(_a) << 24 | \
				(uint32_t)(_b)<<16 | \
				(uint32_t)(_c)<<8 | (_d))


#define MAX_RESP_SIZE		96

struct response_packet{
	uint32_t magic;
	uint16_t size;
	uint8_t data[MAX_RESP_SIZE];
}__attribute__((packed));

#define RESP_MAGIC	MAKE_UL('U', 'S', 'B', 'P')
#define RESP_OVERHEAD	(sizeof(struct response_packet) - MAX_RESP_SIZE)


typedef union _UppPackage
{
	struct _fields
	{
		unsigned cmd:8;
		unsigned size:8;
		unsigned addrU:8;
		unsigned addrH:8;
		unsigned addrL:8;
		unsigned blocktype:8;
		unsigned char dataField[32];
	}f;
	char data[38];
}UppPackage;

#define CMD_ERASE			0x10
#define CMD_READ_ID			0x20
#define CMD_WRITE_CODE			0x30 // 0011 0000
#define CMD_READ_CODE_OLD		0x40
#define CMD_WRITE_DATA			0x50 // 0101 0000
#define CMD_READ_DATA			0x60
#define CMD_WRITE_CONFIG		0x70 // 0111 0000
#define CMD_SET_PICTYPE			0x80
#define CMD_FIRMWARE_VERSION		0x90
#define CMD_DEBUG			0xA0
#define CMD_GET_PIN_STATUS		0xB0
#define CMD_SET_PIN_STATUS		0xC0
#define CMD_GET_PROTOCOL_VERSION 	0xD0	// PROT_UPP1
#define CMD_READ_CODE			0xD1	// PROT_UPP1
#define CMD_READ_CONFIG			0xD2	// PROT_UPP1
#define CMD_MREAD_CODE			0xD4    // PROT_UPP2
#define CMD_APPLY_SETTINGS		0xD5    // PROT_UPP3

/* extra cmds */
#define CMD_SYNC			0xF0
#define CMD_RESET_TARGET		0xF1
/* debug cmds */
#define CMD_TARGET_TEST			0xFA
#define CMD_LINK_TEST			0xFB

#define PIN_PGC 	0x01
#define PIN_PGD		0x02
#define PIN_VDD		0x03
#define PIN_VPP		0x04

/* 3V3 and 12V are not supported */
#define STATE_0V	0x01
#define STATE_5V	0x03
#define STATE_FLOAT	0x05
#define STATE_INPUT	0x06

#define RESET_STATE_ICSP 0x1

#define UPP_OK				1
#define UPP_GET_NEXT_BLOCK		2
#define UPP_NOT_SUPPORTED		3
#define UPP_VERIFY_ERR			4

// dsP30F:29 dsP33F:2a dsP30F_LV:40
#define DSPIC30F_LV		0x40

#define VERSION_STRING		"Usbpicprog-serial 0.3.0"


int do_pic_prog_cmd(uint8_t *data, int size);
#endif // _UPP_H_


