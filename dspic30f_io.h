#ifndef __DSPIC30F_IO_H__
#define __DSPIC30F_IO_H__

#define DEV_ID	0xff0000
#define Si_REV	0xff0002
#define APP_ID	0x8005BE

#define FBS	0xF80000
#define FGS	0xF80004
#define FOSCSEL	0xF80006
#define FOSC	0xF80008
#define FWDT	0xF8000A
#define FPOR	0xF8000C
#define FICD	0xF8000E

#define DSPIC_USER_MEM_START	0
#define DSPIC_USER_MEM_END	0x7ffffe
#define DSPIC_CONFIG_MEM_START	0x800000
#define DSPIC_CONFIG_MEM_END	0xfffffe
#define MAX_ADDR_WORD		DSPIC_CONFIG_MEM_END
#define MIN_FLASH_WR_SIZE	96 /* 32 instructions */


void reset_target();
void enter_icsp_mode();
void clk_out_value(unsigned long instr, int nbits);
void exit_icsp_mode();
void phy_init();

unsigned int target_sync(unsigned int val);
unsigned int read_visi();

unsigned int execute_code(unsigned long *code, int sz);
unsigned int execute_code_outbuff(unsigned long *code, int sz,
						uint16_t *out, int outw);


unsigned int read_user_memory(unsigned long offb, uint8_t *data,
							unsigned int nbytes);
unsigned int read_config_memory(unsigned long offb, uint8_t *data,
							unsigned int nbytes);
uint16_t read_config_word(unsigned long offb);
unsigned int read_program_memory(unsigned long offb, uint8_t *data,
							unsigned int nbytes);

unsigned int target_asr(unsigned int val);
unsigned int target_test(unsigned long val, uint8_t *data, int nbytes);

void erase_all();

void write_config_word(unsigned long dest, uint16_t val);
unsigned int write_user_memory(unsigned long offb, uint8_t *data,
							unsigned int nbytes);


#endif //  __DSPIC30F_IO_H__
