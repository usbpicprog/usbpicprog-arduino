#include "Arduino.h"
#include "log.h"

int dynamic_print = 0;

void set_dynamic_print(int en)
{
	dynamic_print = en;
}

void dump_data(const char *msg, const uint8_t *data, uint32_t off, int size)
{
    int i;
    pr(msg);
    pr(":");
    pr(size);
    for(i=0; i<size; i++) {
	if(i % 16 == 0) {
		prln("");
		pr(off+i, HEX);
		pr(":");
	}

        pr(data[i], HEX);
        pr(" ");

    }
    prln("");
}


