/* ICSP control code defines */
#define ICSP_CODE_SHIFT	0
#define ICSP_CODE_MASK	0xf
#define ICSP_CODE(_cmd)	(((_cmd) >> ICSP_CODE_SHIFT) & ICSP_CODE_MASK)

#define ICSP_DATA_SHIFT	4
#define ICSP_DATA_MASK	0xffffff
#define ICSP_DATA(_cmd)	(((_cmd) >> ICSP_DATA_SHIFT) & ICSP_DATA_MASK)

#define ICSP_CMD(_code, _data) \
		((((unsigned long)(_data) & ICSP_DATA_MASK) << ICSP_DATA_SHIFT) | \
		((_code) & ICSP_CODE_MASK))

/* icsp control codes */
#define EXEC			0
#define REGOUT  		1
#define HOST_MDELAY 		2

/* icsp opcodes */
#define SIX(_instr)	ICSP_CMD(EXEC, _instr)
#define MDELAY(_ms)	ICSP_CMD(HOST_MDELAY, _ms)
#define REG_OUT		ICSP_CMD(REGOUT, 0)


/* icsp control codes */
#define EXEC			0
#define REGOUT  		1
#define HOST_MDELAY 		2

/* register definitions */
#define W0	0
#define W1	1
#define W2	2
#define W3	3
#define W4	4
#define W5	5
#define W6	6
#define W7	7
#define W8	8
#define W9	9
#define W10	10
#define W11	11
#define W12	12
#define W13	13
#define W14	14
#define W15	15

/* instruction helpers */
#define MVI(imm16l, imm16h, wreg) \
				((unsigned long)0x2 << 20) | \
				((unsigned long)(imm16h & 0xff) << 12) | \
				((unsigned long)(imm16l & 0xff) << 4) | \
				(wreg & 0xf)

#define MVI8(imm8, wreg)	MVI(imm8, 0, wreg)
#define MVI16(imm16, wreg)	MVI(imm16, (imm16>>8), wreg)
#define NOP			0

/* instruction word offsets */
#define W0L	0	/* word0 low byte */
#define W0M	1	/* word0 mid byte */
#define W0H	2	/* word0 high byte */

#define W1L	3
#define W1M	4
#define W1H	5

#define W2L	6
#define W2M	7
#define W2H	8

#define W3L	9
#define W3M	10
#define W3H	11

/* NVMCON codes */
#define NVMCON_ERASE_ALL 	0x407F	// Erase everything
#define NVMCON_ERASE_EXECUTIVE	0x4072	// Erases all executive memory.
#define NVMCON_ERASE_ROW	0x4071 	// Erases 1 row (32 instrs)
					// from 1 panel of code memory.
#define NVMCON_WRITE_CONFIG 	0x4008	// Write a config word
#define NVMCON_WRITE_32IWORDS 	0x4001	// Write 32 instruction words


/* assembly routine helpers */
/*
 * Shift out a register value. The register can be W0 to W15
 */
#define CLK_OUT_REG(_reg) \
	SIX(0x883C20|(_reg & 0xF)), /* MOV Wx, VISI */ \
	SIX(NOP), \
	REG_OUT, /* VISI out */\
	SIX(NOP)

static inline void swap_byte(uint8_t *a, uint8_t *b);

#define UNPACK_INSTR_WORDS(data) \
	swap_byte(&data[W1L], &data[W1M]); \
	swap_byte(&data[W1M], &data[W1H]); \
	swap_byte(&data[W3L], &data[W3M]); \
	swap_byte(&data[W3M], &data[W3H]); \

static inline void swap_byte(uint8_t *a, uint8_t *b)
{
	uint8_t c;
	c = *b;
	*b = *a;
	*a = c;
}

