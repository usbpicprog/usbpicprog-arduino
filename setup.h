#include "Arduino.h"
#include "log.h"

#ifdef TEST_PHY /* large delay to analyze the phy */
#define nanosec_50()  delayMicroseconds(100)
#else
#define nanosec_50()  __asm__("nop\n\t") /* @16MHz, 62.5ns */
#endif

#define nanosec_100()  nanosec_50(); nanosec_50()

#define nanosec_500()	nanosec_100(); \
			nanosec_100(); \
			nanosec_100(); \
			nanosec_100()

#define mdelay(_m)  delay(_m)

/* setup definitions */
#define PGC	8
#define PGD	9
#define MCLR	10

#define data_low()	digitalWrite(PGD, LOW)
#define data_high() 	digitalWrite(PGD, HIGH)
#define data_output() 	pinMode(PGD, OUTPUT)
#define data_tristate() pinMode(PGD, INPUT)
#define data_read(_d)	digitalRead(PGD)
#define data_write(_d)	digitalWrite(PGD, _d)

#define clock_low() 	digitalWrite(PGC, 0)
#define clock_high() 	digitalWrite(PGC, 1)
#define clock_output()	pinMode(PGC, OUTPUT);

#define reset_board()   digitalWrite(MCLR, LOW)
#define remove_reset()	digitalWrite(MCLR, HIGH)
#define reset_output()	pinMode(MCLR, OUTPUT);

#define clock_delay()	nanosec_50()

/* need to match this with the programmer */
#define SERIAL_BAUD	115200
