#include "setup.h"
#include "helpers.h"

#include "dspic30f_io.h"
#include "upp.h"

void show_device_info()
{
        prinfo("Device id: ", read_config_word(DEV_ID));
        prinfo("Chip rev: ", read_config_word(Si_REV));
        prinfo("App id: ", read_config_word(APP_ID));
}

void show_device_config()
{
        prinfo("Boot segment cfg: ", read_config_word(FBS));
        prinfo("Gen  segment cfg: ", read_config_word(FGS));
        prinfo("Oscillator cfg: ", read_config_word(FOSC));
        prinfo("Oscillator selector: ", read_config_word(FOSCSEL));
        prinfo("WDT config: ", read_config_word(FWDT));
        prinfo("Power on reset config: ", read_config_word(FPOR));
        prinfo("Incircuit debugger config: ", read_config_word(FICD));
}

void setup()
{
	Serial.begin(SERIAL_BAUD);
	prln("Starting programmer");
	phy_init();
	reset_target();

	show_device_info();
	show_device_config();
	prln("Waiting for serial commands");
}


void loop()
{
	int writeb;
	uint8_t *cmd;

	struct response_packet r;
	cmd = r.data;
	r.magic = RESP_MAGIC;
	/* sync with the host */
	/* listen for serial commands from programmer */
        if (Serial.available() > 0) {
		cmd[0] = Serial.read();
		writeb = do_pic_prog_cmd(cmd, MAX_RESP_SIZE);
		if(writeb) {
			r.size = writeb;
			dump_data("serialwrite", (uint8_t *)&r, 0,
						RESP_OVERHEAD + writeb);
			/* r.data will be already filled in do_pic_prog_cmd */
			Serial.write((uint8_t *)&r, RESP_OVERHEAD + writeb);
		}
        }
}
