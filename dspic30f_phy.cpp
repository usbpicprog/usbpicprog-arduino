#include "Arduino.h"

#include "setup.h"
#include "helpers.h"

#include "dspic30f_asm.h"
/* to clkout a nbit value in 1Mhz clk */
void clk_out_value(unsigned long instr, int nbits)
{
	int b, i;
#ifdef DEBUG_SIGNAL
	pr("clk out:");
	pr(instr, HEX);
	pr(":");
	pr(nbits, DEC);
	pr(":");
#endif
	data_low();
	for(i=0; i<nbits; i++) {
		b = instr & 1;
		instr >>= 1;
		/* transmit on rising edge */
		data_write(b);
#ifdef DEBUG_SIGNAL
		pr(b);
#endif
		clock_high();
		clock_delay();
		clock_low();
		clock_delay();
  	}
#ifdef DEBUG_SIGNAL
	prln("b");
#endif
	data_low();
}

unsigned int read_visi()
{
	unsigned int val;
	int i;
	clk_out_value(0x1, 12);
	nanosec_50();
  	data_tristate();
	val = 0;
	/* read a 16bit value */
	for(i=0; i<16; i++) {
		/* latch on falling edge */
		clock_high();
		clock_delay();
		val |= ((unsigned int)data_read()) << i;
		clock_low();
		clock_delay();
  	}
	data_output();
	data_low();
#ifdef DEBUG_SIGNAL
	prhex("VISI=", val);
#endif
	return val;
}

void reset_to_icsp_mode()
{
	reset_board();
	nanosec_100(); // P6
	remove_reset();
	mdelay(1);    // P12 ?
	reset_board();
	nanosec_50();  // P16
	clk_out_value(0x8A12C2B2, 32);
	nanosec_50(); // P17
	remove_reset();
	mdelay(1); // P7
}

unsigned int execute_code_outbuff(unsigned long *code, int sz,
						uint16_t *out, int outw)
{
	int i, j;
        unsigned int icsp_code;

	prln(__func__);
	for(i=0, j=0; i < sz && j < outw; i++) {
		icsp_code = ICSP_CODE(code[i]);
		if(icsp_code == EXEC) {
			prhex("SIX:", code[i]);
			clk_out_value(code[i], 28);
		} else if(icsp_code == REGOUT) {
			prln("REG_OUT");
			out[j++] = read_visi();
		} else if(icsp_code == HOST_MDELAY) {
			prinfo("mdelay:", ICSP_DATA(code[i]));
			mdelay(ICSP_DATA(code[i]));
		} else {
			prinfo("Unknown icsp control code:", icsp_code);
		}

	}
	prln("END");
	return j;
}

unsigned int execute_code(unsigned long *code, int sz)
{
	int i;
        unsigned int val = 0, icsp_code;
	prln(__func__);
	for(i=0; i<sz; i++) {
		icsp_code = ICSP_CODE(code[i]);
		if(icsp_code == EXEC) {
			prhex("SIX:", code[i]);
			clk_out_value(code[i], 28);
		} else if(icsp_code == REGOUT) {
			val = read_visi();
			prhex("REG_OUT:", val);
		} else if(icsp_code == HOST_MDELAY) {
			prinfo("mdelay:", ICSP_DATA(code[i]));
			mdelay(ICSP_DATA(code[i]));
		} else {
			prinfo("Unknown icsp control code:", icsp_code);
		}

	}
	prln("END");
	return val;
}

/* defined as program reset */
void post_reset_init()
{
	data_low();
	clk_out_value(0, 5);
	nanosec_50(); // P4
}


void reset_target()
{
	prln(__func__);
	clock_low();
	data_low();
	reset_board();
	nanosec_50();
	remove_reset();
}

void enter_icsp_mode()
{
	prln(__func__);
	reset_to_icsp_mode();
	post_reset_init();
}

void exit_icsp_mode()
{
	reset_target();
}

void phy_init()
{
	clock_output();
	data_output();
	reset_output();
}
