#include "Arduino.h"

#include "helpers.h"
#include "dspic30f_asm.h"
#include "dspic30f_io.h"

#ifdef TEST_CMD
/*
 * used to test the target, especially with the read user memory commands.
 * The src value is any  16-bit value and the output will be
 * 12 bytes with each 16b holding, src, src+1, src+2, src+3, src+4, src+5
 */
#define TARGET_TEST_6W(_src) \
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(0x040100),	/* GOTO 0x100 */ \
	SIX(NOP), \
	\
	SIX(MVI16((_src), W0)),   \
	SIX(MVI16((_src+1), W1)), \
	SIX(MVI16((_src+2), W2)), \
	SIX(MVI16((_src+3), W3)), \
	SIX(MVI16((_src+4), W4)), \
	SIX(MVI16((_src+5), W5)), \
	\
	CLK_OUT_REG(W0), \
	CLK_OUT_REG(W1), \
	CLK_OUT_REG(W2), \
	CLK_OUT_REG(W3), \
	CLK_OUT_REG(W4), \
	CLK_OUT_REG(W5), \
	SIX(NOP), \
	SIX(0x40100), \
	SIX(NOP)

/*
 * Do the target test with val.
 * val: any 16 bit value
 * data: data buffer
 * nbytes: must be a multiple of 12 and min value of 12
 */
unsigned int target_test(unsigned long val, uint8_t *data, int nbytes)
{
	int i;
	unsigned long instrs[] = {
		TARGET_TEST_6W(val)
	};

	for(i=0; i < nbytes/12; i++) {
		execute_code_outbuff(instrs,
					ARRAY_SIZE(instrs),
					/* ofssets and size in words */
					(uint16_t*)data + i*6, 6);
	}

	return i*12;
}

#endif

