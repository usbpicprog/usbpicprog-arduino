# you could change the base dir as required
ARDUINO_DIR   = /usr/share/arduino
# Testing flags
# Used to test the target and the command link
#DEFINES += -DTEST_CMD
# used to test signalling with a low BW scope like sound-card
#DEFINES += -DTEST_PHY
CPPFLAGS += -W $(DEFINES)
MONITOR_PORT=/dev/ttyACM0
include $(ARDUINO_DIR)/Arduino.mk
