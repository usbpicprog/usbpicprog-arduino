#include "Arduino.h"
#include "setup.h"
#include "helpers.h"

#include "dspic30f_io.h"
#include "upp.h"

uint8_t cmd_next_byte()
{
        while(1) {
		if(Serial.available() > 0)
			return Serial.read();
	}
}

void cmd_next_nbytes(uint8_t *buf, int sz)
{
	int i = 0;
        while(i < sz) {
		if(Serial.available() > 0)
			buf[i++] = Serial.read();
	}
}

int do_cmd_io(int cmd, uint8_t nbytes, uint32_t start_addr,
					uint8_t block_type, uint8_t *data)
{
	int ret;

	prinfo("block size:", nbytes);
	prhex("start addr", start_addr);
	prinfo("block type", block_type);

	switch(cmd) {
#ifdef TEST_CMD
	static int cnt = 0;
	/* used to test the arduino-serial link */
	case CMD_LINK_TEST:
		// link test
		if(block_type == 1)
			cnt = 0;
		cnt++;
		memset(data, cnt, nbytes);
		ret = nbytes;
		dump_data("linktest", data, start_addr, nbytes);
		break;
	case CMD_TARGET_TEST:
	/* used to test the target */
		ret = target_test(start_addr, data, nbytes);
		/* convert to byte size */
		dump_data("targettest", data, start_addr, nbytes);
		break;
#endif

	case CMD_WRITE_CODE:
		dump_data("writecode", data, start_addr, nbytes);
		ret = write_user_memory(start_addr, data, nbytes);
		if(ret == nbytes)
			data[0] = (block_type == 2)? UPP_OK:UPP_GET_NEXT_BLOCK;
		else
			data[0] = UPP_NOT_SUPPORTED;
		ret = 1;
		break;

	case CMD_WRITE_CONFIG:
		write_config_word(start_addr, *(uint16_t *)data);
		data[0] = (block_type == 2)? UPP_OK:UPP_GET_NEXT_BLOCK;
		ret = 1;
		break;

	case CMD_READ_CODE:
		ret = read_program_memory(start_addr, data, nbytes);
		dump_data(__func__, data, start_addr, ret);
		break;

	default:
		ret = 0;
		break;

	}
	return ret;
}

int do_pic_prog_cmd(uint8_t *data, int size)
{
	int val, ret = 0;
	uint8_t cmd;

	if(size < MAX_RESP_SIZE)
		return -1;

	val = 0;
	cmd = data[0];

	prhex("UPP command: ", cmd);
	switch(cmd) {
	case CMD_READ_ID:
        	val = read_config_word(0xff0000);
		prhex("ReadID:", val);
		data[0] = val & 0xff;
		data[1] = (val & 0xff00) >> 8;
		ret = 2;
		break;

	case CMD_WRITE_CONFIG:
	case CMD_WRITE_CODE:
		val = 1;
		/* fall through */
	case CMD_READ_CODE:
	case CMD_TARGET_TEST:
	case CMD_LINK_TEST:
		uint8_t block_sz, block_type;
		uint32_t start_addr;

		start_addr = 0;
		block_sz = cmd_next_byte();
		start_addr = ((uint32_t)cmd_next_byte())<<16;
		start_addr |= ((uint32_t)cmd_next_byte() << 8);
		start_addr |= cmd_next_byte();
		block_type = cmd_next_byte();
		if(val) /* non-write commands terminate here */
			cmd_next_nbytes(data, block_sz);
		/* do the IO command */
		ret = do_cmd_io(cmd, block_sz, start_addr, block_type, data);
		break;


	case CMD_ERASE:
		erase_all();
		data[0] = UPP_OK;
		ret = 1;
		break;

	case CMD_SET_PICTYPE:
		val = cmd_next_byte();
		if(val == DSPIC30F_LV) {
			data[0] = UPP_OK;
		} else {
			data[0] = UPP_NOT_SUPPORTED;
		}
		ret = 1;
		break;

	case CMD_GET_PROTOCOL_VERSION:
		data[0] = 0;
		ret = 1;
		break;

	case CMD_FIRMWARE_VERSION:
		strcpy((char *)data, VERSION_STRING);
		ret = sizeof(VERSION_STRING);
		break;

	case CMD_SYNC:
		/*
		 * Get target to reply for the command to
		 * see if the target chip works.
		 */
		uint8_t lb, hb;
		lb = cmd_next_byte(),
		hb = cmd_next_byte();

		*((uint16_t*)data) = target_asr(hb<<8|lb);
		ret = 2;
		break;

	case CMD_RESET_TARGET:
		/* the status of reset should be checked using CMD_SYNC */
		val = cmd_next_byte();
		if(val == RESET_STATE_ICSP)
			enter_icsp_mode();
		else
			reset_target();
		/* no return */
		break;

	case CMD_DEBUG:
		set_dynamic_print(cmd_next_byte());
		prln("Dynamic print");
		break;

	case CMD_SET_PIN_STATUS: /* to run/stop target */
	{
		uint8_t pin = cmd_next_byte();
		uint8_t state = cmd_next_byte();

		data[0] = UPP_NOT_SUPPORTED;
		ret = 1;
		if(pin == PIN_VPP) {
			/* use VPP = 5V as exit */
			if (state == STATE_5V) {
				exit_icsp_mode();
				data[0] = UPP_OK;
			} else if (state == STATE_FLOAT) {
				enter_icsp_mode();
				data[0] = UPP_OK;
			}
		} else if(pin == PIN_VDD || pin == PIN_PGC || pin == PIN_PGD) {
			if(state == STATE_5V || state == STATE_INPUT ||
				state == STATE_FLOAT || state == STATE_0V) {
				data[0] = UPP_OK;
			}
			/* 3V3 and 12V are not supported */
		}
		break;
	}

	default:
		prhex("Unknown command\n", cmd);
		break;

	}
	return ret;
}

